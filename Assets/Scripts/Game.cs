﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class Game : MonoBehaviour
{
    public Collider Screen;
    public VideoPlayer videoPlayer;
    public int width;
    public int height;
    public Otto PreOtto;

    private Otto[][] list = new Otto[0][];

    public Texture2D temp;

    public virtual void Start()
    {
        videoPlayer.sendFrameReadyEvents = true;
        videoPlayer.frameReady += VideoPlayerOnFrameReady;
        Bounds screenBounds = Screen.bounds;
        PreOtto.gameObject.SetActive(false);
        Array.Resize(ref list, width);
        for (var x = 0; x < list.Length; x++)
        {
            Array.Resize(ref list[x], height);
            for (var y = 0; y < list[x].Length; y++)
            {
                Vector3 pos = Vector3.zero;
                pos.x = screenBounds.size.x * x / (width - 1);
                pos.z = screenBounds.size.z * y / (height - 1);
                pos.y = 1;
                list[x][y] = GameObject.Instantiate(PreOtto.gameObject, pos, Quaternion.identity, transform).GetComponent<Otto>();
                list[x][y].gameObject.SetActive(true);
            }
        }

    }

    private void VideoPlayerOnFrameReady(VideoPlayer source, long frameidx)
    {
        RenderTexture renderTexture = source.texture as RenderTexture;
        if (temp == null)
        {
            temp = new Texture2D(renderTexture.width, renderTexture.height);
        }
        RenderTexture.active = renderTexture;
        temp.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        temp.Apply();
        RenderTexture.active = null;
        for (int x = 0; x < list.Length; x++)
        {
            for (var y = 0; y < list[x].Length; y++)
            {
                float u = ((float)x) / list.Length;
                float v = ((float)y) / list[x].Length;

                Color color = temp.GetPixelBilinear(u, v);
                list[x][y].PutHat(color.grayscale > 0.5f);

            }
        }
    }
}