﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class Otto : MonoBehaviour
{
    private Animator animator;
    public virtual void Awake()
    {
        animator = GetComponent<Animator>();
    }



    public void PutHat(bool b)
    {
        animator.SetBool("Put", b);
    }
}